import cv2
import numpy as np

def dibujar(mask,color):
    contornos,hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    for c in contornos:
        area=cv2.contourArea(c)
        if area > 700:
            nuevoContorno=cv2.convexHull(c)
            #cv2.drawContours(image, [nuevoContorno], 0, color ,3)
            cv2.drawContours(frame, [nuevoContorno], 0, color, 3)

cap = cv2.VideoCapture(0)
#image=cv2.imread('images/logoulagossinfondo.png',1)

verdeBajo = np.array([46,100,20],np.uint8)
verdeAlto = np.array([75,255,255],np.uint8)

azulBajo = np.array([100,100,20],np.uint8)
azulAlto = np.array([125,255,255],np.uint8)

amarilloBajo = np.array([15,100,20],np.uint8)
amarilloAlto = np.array([45,255,255],np.uint8)

redBajo1 = np.array([0,100,20],np.uint8)
redAlto1 = np.array([5,255,255],np.uint8)

redBajo2 = np.array([175,100,20],np.uint8)
redAlto2 = np.array([179,255,255],np.uint8)

#font = cv2.FONT_HERSHEY_SIMPLEX

#Prueba con imagen

while True:
    ret,frame = cap.read()

    if ret==True:
         #frameHSV = cv2.cvtColor(image,cv2.COLOR_BGR2HSV)
         frameHSV = cv2.cvtColor(frame,cv2.COLOR_RGB2HSV)
         maskAzul = cv2.inRange(frameHSV,azulBajo,azulAlto)
         maskAmarillo = cv2.inRange(frameHSV,amarilloBajo,amarilloAlto)
         maskRed1 = cv2.inRange(frameHSV,redBajo1,redAlto1)
         maskRed2 = cv2.inRange(frameHSV,redBajo2,redAlto2)
         maskRed = cv2.add(maskRed1,maskRed2)
         maskVerde = cv2.inRange(frameHSV,verdeBajo,verdeAlto)
         dibujar(maskAzul,(0,0,255))
         dibujar(maskAmarillo,(0,255,255))
         dibujar(maskRed,(255,0,0))
         dibujar(maskVerde,(0,255,0))
         cv2.imshow('frame',frame)
        # cv2.imshow('frame',image)
         if cv2.waitKey(1) & 0xFF == ord('s'):
             break
cap.release()
cv2.destroyAllWindows()


