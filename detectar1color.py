import cv2
import numpy as np

cap = cv2.VideoCapture(0)

redBajo1 = np.array([0,100,20],np.uint8)
redAlto1 = np.array([5,255,255],np.uint8)

redBajo2 = np.array([175,100,20],np.uint8)
redAlto2 = np.array([179,255,255],np.uint8)

while True:
    ret,frame=cap.read()
    if ret==True:
        frameHSV=cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
        maskRed1= cv2.inRange(frameHSV,redBajo1,redAlto1)
        maskRed2=cv2.inRange(frameHSV,redBajo2,redAlto2)
        maskRed=cv2.add(maskRed1,maskRed2)

        contornos,hierarchy = cv2.findContours(maskRed,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
        for c in contornos:
            area=cv2.contourArea(c)
            if area > 700:
                nuevoContorno=cv2.convexHull(c)
                cv2.drawContours(frame, [nuevoContorno], -1, (0,0,255), 3)
        #cv2.imshow('maskRed',maskRed)
        cv2.imshow('frame',frame)
        if cv2.waitKey(1) & 0xFF == ord('s'):
            break

cap.release()
cv2.destroyAllWindows()
